/*************************************************************************
 *  Copyright © 2023-2030 Administrator. All rights reserved.
 *------------------------------------------------------------------------
 *  文件：ScriptHeadComments.cs
 *  作者：Administrator
 *  日期：2024/7/4 16:56:26
 *  公司：Fxb
 *  项目：com.fxb.scriptheadcomments
 *  功能：Nothing
*************************************************************************/

using System;
using UnityEditor;
using UnityEngine;

namespace ScriptHeadComments.Editor
{
	public class ScriptHeadComments : ScriptableObject
	{
        [SerializeField]
        [HideInInspector]
        private bool isInitialized;
        public string copyright;
        public string authorName;
        public string assembleName;

        private void OnEnable()
        {
            if (!isInitialized)
            {
                copyright = "FXB CO.,LTD";
                authorName = Environment.UserName;
                assembleName = "NAMESPACE";
                isInitialized  = true;
            }
        }


        public static void CreateBuildInfoAsset(string assetPath)
        {
            ScriptHeadComments asset = ScriptableObject.CreateInstance<ScriptHeadComments>();
            AssetDatabase.CreateAsset(asset, assetPath);
            AssetDatabase.SaveAssets();
        }
    }
}

